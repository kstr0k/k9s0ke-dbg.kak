# Kakoune debug commands / aliases

## _Convenient kakoune developer helpers_

Most of the aliases minimize keystroke count / finger movement. They also avoid polluting precious map key-space.

## Install

If using [`plug.kak`](https://github.com/andreyorst/plug.kak):
```
# Load this early to use debug commands from other defs in your code
plug https://gitlab.com/kstr0k/k9s0ke-dbg %{
} demand k9s0ke-dbg {
  k9s0ke-dbg-def-aliases
  k9s0ke-dbg-def-aliases-funny  # syntax hacks like ':p', '=.'
}
```

Manual installation: place somewhere in `autoload`, and `require-module k9s0ke-dbg` in `kakrc`.


## Commands / aliases

Note that the aliases (except `defp`) will not be loaded by default, until you call `k9s0ke-dbg-def-aliases...` as above. Thus, the module won't destroy any existing user aliases.

### Display

- `e'' ARG..` (`edit-string`): display arguments in a `*string*` scratch buffer. Example: `e'' %val{client_env_PATH}`.
- `e* command ARG..` (`edit-dbg`): capture `*debug*` output from any command, and copy it to a scratch `*output*` buffer. Preserves command completion. Example: `e* debug registers`. If you're debugging something that consistently refuses to run and produces copious `*debug*`, `edit-dbg` switches you directly to the output and can be easily repeated with `:<up><ret>`. Like vim `bufferize`.
- `:p ARG..` (or `echo-dbg-plstar`): prints out compactly all args using Perl's `Data::Dumper`. For example, newlines are shown as `\n` in strings. Use it in a command to debug arguments (`echo-dbg-plstar %arg{@}` at the top of your command definition) or from the command line to debug list options (`:p %opt{ui_options}`).
- `:?`: simple `echo` (to the status line), with all advantages / limitations.
- `cat-dbg`: `cat` a file to the `*debug*` buffer.
- `:'`, `:\'`: like `echo -debug -quoting [ kakoune | shell ]`
- `echo-dbg-...star`: more ways to dump arguments. See code / try them out.
- `:`: not defined &mdash; just a suggestion; pick your choice from the above. This is the most convenient `echo` alias, as you can type `::<space>` to enter command mode and start the `echo` command.

### Programming

- `defp command-name %{ code-here %}`: quickly define a command that takes any number of arguments and uses its *own code* as a `-docstring`. Alias for `def-params-dotdot`
- `alias-args`: alias not just a command (like `alias`), but also some prefix arguments. E.g. `setg+` is defined as `alias-args setg+ set -add global`. Generates a hidden function helper to achieve this. The `-docstring` is auto-generated. There's one disadvantage: it bypasses the builtin `alias` completion (which seems impossible to recover).

### Execution

- `nop-sh sh-code $1-arg $2-arg..`: like `nop %sh{}`, but takes explicit script arguments instead of deriving them implicitly from the surrounding command. Also, standard output is redirected to `stderr`, so any `echo / printf` output goes to Kakoune's `*debug*`. Use it like this: `nop-sh %{echo $1; ls $2} %val{session} .`
- `eval-sh sh-code ARG..`: like `eval -- %sh{}`, but also with explicit args. No redirections, since output is meant to be evaluated.
- `eval-pl perl-code ARG..`: like `eval-sh`, but for inline perl scripts
- `nop-plp`: like `nop perl -pe` with the `nop-sh` hacks and file tab-completion. E.g. `nop-plp /etc/passwd s/:/-/g`.

### Convenience

- `set+`, `set-`, `setg[+|-]`: like `set [-add | -remove] [global]`. Defined using `alias-args`.
- `source-me`: evaluate file as Kakoune code (presumably you're editing a `*.kak` file).
- `=.` (`eval-selections`): evaluate selected lines as Kakoune code.
- `nnopp`: `nop %sh{}` actually breaks if the output starts with a dash, because it's confused as a (non-existent) `nop` switch. This is `nop --`.

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license
