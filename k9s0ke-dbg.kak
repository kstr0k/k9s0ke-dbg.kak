provide-module k9s0ke-dbg %{

def def-params-dotdot -params .. %{
  def -override -params .. %arg{@} -docstring "%%{}:
%arg{2}"
} -docstring %{= def -params.. -docstring <code>} -override
alias global defp def-params-dotdot  # used below

def -override setglobal -params 3 %{ set global %arg{@} } -docstring %{set global ...}

defp nnopp %{ nop -- %arg{@} }
defp eval-sh %{ eval eval -- "%%sh{
: '{{{{{'; shift; %arg{1}
: '}}}}}'
}" }
defp nop-sh %{ eval nop -- "%%sh{
: '{{{{{'; exec 3>&1 1>&2; shift; %arg{1}
: '}}}}}'
}" }
defp eval-pl %{ eval-sh %{
s=$1; shift; exec perl -Mstrict -wE "$s" -- "$@"
} %arg{@} }
defp nop-plp %{ eval-sh %{
exec <"$1"; s=$2; shift 2; exec perl 3>&1 1>&2 -Mstrict -wpE "$s" -- "$@"
} %arg{@} } -file-completion

defp echo-dbg-shdump %{ nop -- %sh{exec 1>&2
echo "-- \$#=$#:"
i=1; while [ $i -le $# ]; do
eval a=\$$i
printf '$%s: |%s|\n' "$i" "$a"; i=$(( i + 1 ))
done
}}
defp echo-dbg-kkq %{ echo -debug -quoting kakoune -- %arg{@} }
defp echo-dbg-shq %{ echo -debug -quoting shell -- %arg{@} }
defp echo-dbg-raw %{ echo -debug -quoting raw -- %arg{@} }
defp echo-dbg-kkstar %{ echo -debug '%arg{@} =' "<%arg{@}>" }
defp echo-dbg-shstar %{ nop -- %sh{ exec 1>&2; printf '$* = <'; printf '%s' "$*"; printf '>'; }}
defp echo-dbg-plstar %{ nop -- %sh{ exec perl 1>&2 -wE '
use Data::Dumper; $Data::Dumper::Useqq=1; #$Data::Dumper::Terse=1;
say "\@ARGV[ 0..$#ARGV ] ="; print Dumper @ARGV' "$@" } }

defp cat-dbg %{ eval -- echo-dbg-raw "%%file{%arg{1}}" } -file-completion

def edit-string -params .. %{
  edit -scratch *string*
  eval -save-regs b %{
    reg b "%arg{@}"
    exec -draft 'ge"bPo'
  }
} -docstring %{
Show all stringified arguments in scratch buffer *string*
E.g.:
  edit-string %opt{ui_options}
  edit-string %val{client_env_PATH}
} -override

def edit-dbg -params 1.. %{
  eval -save-regs by %{
    buffer *debug*
    echo -debug "Running command: %arg{@}"
    exec 'gek"yZ'
    %arg{@}
    buffer *debug*
    exec '"yzGe"by'
    edit -scratch *output*
    exec '<esc>ge"bP'
  }
} -docstring %{
Run a command that produces *debug* output, show it in an *output* scratch
E.g.: edit-dbg debug registers
} -override -command-completion

defp source-me %{ exec ': w<ret>'; source %val{buffile} %arg{@} }

def eval-selections -docstring %{eval selection lines} %{
  eval -draft %{ exec '<a-x>'; eval %reg{.} }
} -override

def eval-buffer -docstring %{eval entire buffer} %{
  eval -draft %{ exec ggGe; eval %reg{.} }
} -override

decl int k9s0ke_dbg_alias_args_cnt 0
defp alias-args %{
  set -add global k9s0ke_dbg_alias_args_cnt 1
  def "%arg{2}-alias-args-helper%opt{k9s0ke_dbg_alias_args_cnt}" "%sh{
    shift; printf %s ""$*""
  } %%arg{@}" -docstring "alias-args: %arg{@}" -override -hidden -params ..
  alias global %arg{1} "%arg{2}-alias-args-helper%opt{k9s0ke_dbg_alias_args_cnt}"
}

defp k9s0ke-dbg-def-aliases %{
  alias-args setg  set         global
  alias-args setg+ set -add    global
  alias-args setg- set -remove global
  alias-args set+  set -add
  alias-args set-  set -remove
}

defp k9s0ke-dbg-def-aliases-funny %{
alias global :p    echo-dbg-plstar
alias global :?    echo
alias global ":'"  echo-dbg-kkq
alias global :\'   echo-dbg-kkq
alias global "e''" edit-string
alias global "e*"  edit-dbg

alias global kko   echo

alias global =.    eval-selections
}

}  # module
